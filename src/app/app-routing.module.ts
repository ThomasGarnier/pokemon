import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found.component';

// routes
const appRoutes: Routes = [
    { path: '', redirectTo: 'pokemons', pathMatch: 'full' },
    // **: permet dintercepter toutes les routes
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        // forRoot : indiqué uniquement pour les routes au niveau du module racine
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
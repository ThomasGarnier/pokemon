import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { PokemonsModule } from './pokemons/pokemons.module';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found.component';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

// NgModule peut avoir 5 propriétés de module :
// - imports : classes exportées nécessaires au fonctionnement du module
// - declarations : 3 types de classes de vues : Composants, Directives et Pipes
// - exports : sous ensemble de classes (déclarations) de vues à exporter ds d'autres modules
// - providers : permet de fournir un service (et injection de dépendance) nécessaire au module 
// - bootstrap : le composant racine pour le module racine (composant au lancement de l'appli)

@NgModule({
	imports: [
		BrowserModule,
		HttpClientModule,
		FormsModule,
		// Le module HttpClientInMemoryWebApiModule intercepte les requêtes HTTP
		// et retourne des réponses simulées du serveur.
		// Retirer l'importation quand un "vrai" serveur est prêt pour recevoir des requêtes.
		HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false }),
		PokemonsModule,
		AppRoutingModule
	],
	declarations: [
		AppComponent,
		PageNotFoundComponent
	],
	providers: [Title],
	bootstrap: [AppComponent]
})
export class AppModule { }

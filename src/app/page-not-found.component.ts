import { Component } from '@angular/core';

@Component({
	selector: 'page-404',
	template: `
  <div class='row mt-4'>
    <div class='col s12 m4 offset-m4'>
      <div class='card'>
        <div class='center'>
          <img src='./assets/img/035.png' class='pokemon-error center'/>
          <p class='bolder'>Désolé cette page n'existe pas !</p>
          <a routerLink='/pokemons/' class='waves-effect waves-teal btn-large blue darken-1 mb-2'>
            Retourner à l' accueil
          </a>
        </div>
      </div>
    </div>
  </div>
  `
})
export class PageNotFoundComponent { }
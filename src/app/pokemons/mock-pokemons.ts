import { Pokemon } from './pokemon';

export const POKEMONS: Pokemon[] = [
	{
		id: 1,
		name: "Bulbizarre",
		hp: 25,
		cp: 5,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
		description: "Il y a une graine sur son dos depuis sa naissance. Elle grossit un peu chaque jour.",
		types: ["Plante", "Poison"],
		created: new Date()
	},
	{
		id: 2,
		name: "Salamèche",
		hp: 28,
		cp: 6,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/004.png",
		description: "Il préfère ce qui est chaud. En cas de pluie, de la vapeur se forme autour de sa queue.",
		types: ["Feu"],
		created: new Date()
	},
	{
		id: 3,
		name: "Carapuce",
		hp: 21,
		cp: 4,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/007.png",
		description: "Quand il rentre son cou dans sa carapace, il peut projeter de l'eau à haute pression.",
		types: ["Eau"],
		created: new Date()
	},
	{
		id: 4,
		name: "Aspicot",
		hp: 16,
		cp: 2,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/013.png",
		description: "L'aiguillon sur son front est très pointu. Il se cache dans les bois et les hautes herbes.",
		types: ["Insecte", "Poison"],
		created: new Date()
	},
	{
		id: 5,
		name: "Roucool",
		hp: 30,
		cp: 7,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/016.png",
		description: "De nature très docile, il préfère projeter du sable pour se défendre plutôt que contre-attaquer.",
		types: ["Normal", "Vol"],
		created: new Date()
	},
	{
		id: 6,
		name: "Rattata",
		hp: 18,
		cp: 6,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/019.png",
		description: "Il peut ronger n'importe quoi avec ses deux dents. Il est toujours accompagné.",
		types: ["Normal"],
		created: new Date()
	},
	{
		id: 7,
		name: "Piafabec",
		hp: 14,
		cp: 5,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/021.png",
		description: "Incapable de voler à haute altitude. Il se déplace très vite pour protéger son territoire.",
		types: ["Normal", "Vol"],
		created: new Date()
	},
	{
		id: 8,
		name: "Abo",
		hp: 16,
		cp: 4,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/023.png",
		description: "Plus il est âgé, plus son corps est long. La nuit, il s'enroule autour de branches d'arbres.",
		types: ["Poison"],
		created: new Date()
	},
	{
		id: 9,
		name: "Pikachu",
		hp: 21,
		cp: 7,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/025.png",
		description: "Plus la puissance électrique de Pikachu est élevée, plus les poches de ses joues sont extensibles.",
		types: ["Electrik"],
		created: new Date()
	},
	{
		id: 10,
		name: "Sabelette",
		hp: 19,
		cp: 3,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/027.png",
		description: "Il aime se rouler dans le sable des zones désertiques pour se débarrasser des traces de terre.",
		types: ["Normal"],
		created: new Date()
	},
	{
		id: 11,
		name: "Mélofée",
		hp: 25,
		cp: 5,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/035.png",
		description: "On dit que ceux qui voient danser un groupe de Mélofée sous la pleine lune connaîtront un grand bonheur.",
		types: ["Fée"],
		created: new Date()
	},
	{
		id: 12,
		name: "Rondoudou",
		hp: 17,
		cp: 8,
		picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/039.png",
		description: "Il a une des meilleures capacités pulmonaires. Il chante des berceuses jusqu'à ce que sommeil s'ensuive.",
		types: ["Fée", "Normal"],
		created: new Date()
	}
];
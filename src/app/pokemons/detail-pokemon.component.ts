import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Pokemon } from './pokemon';
import { PokemonsService } from './pokemons.service';

@Component({
	selector: 'detail-pokemon',
	templateUrl: './app/pokemons/detail-pokemon.component.html'
})
export class DetailPokemonComponent implements OnInit {

	pokemon: Pokemon = null;

	constructor(
		private route: ActivatedRoute, 
		private router: Router,
		private pokemonsService: PokemonsService) {}

	ngOnInit(): void {
		// Récupère le pokémon avec l'identifiant passé depuis PokemonsService
		let id = +this.route.snapshot.params['id'];
		this.pokemonsService.getPokemon(id)
			.subscribe(pokemon => this.pokemon = pokemon);
	}

	goBack(): void {
		this.router.navigate(['/pokemons']);
        // Ou méthode JS pour rediriger en utilisant les propriétés du navigateur
        // window.history.back();
	}

	goEdit(pokemon: Pokemon): void {
		let link = ['/pokemon/edit', pokemon.id];
		this.router.navigate(link);
	}

	delete(pokemon: Pokemon): void {
		this.pokemonsService.deletePokemon(pokemon)
			.subscribe(_ => this.goBack());
	}

}
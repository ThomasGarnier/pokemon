export class Pokemon {
	id: number;
	hp: number;
	cp: number;
	name: string;
	picture: string;
	description: string;
	types: Array<string>;
	created: Date;
}

import { Injectable } from '@angular/core';

import { Pokemon } from './pokemon';
import { POKEMONS } from './mock-pokemons';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class PokemonsService {    
	
	// Point d'acces de l'API (à un seul endroit dans notre service)
	private pokemonsUrl = 'api/pokemons';

  	constructor(private http: HttpClient) { }

	/** GET pokemon Retourne tous les pokémons */
    getPokemons(): Observable<Pokemon[]> {
		return this.http.get<Pokemon[]>(this.pokemonsUrl).pipe(
		  tap(_ => this.log(`fetched pokemons`)),
		  catchError(this.handleError(`getPokemons`, []))
		);
	  }

	/** GET pokemon Retourne le pokémon avec l'identifiant passé en paramètre */
	getPokemon(id: number): Observable<Pokemon> {

	const url = `${this.pokemonsUrl}/${id}`;

	return this.http.get<Pokemon>(url).pipe(
		tap(_ => this.log(`fetched pokemon id=${id}`)),
		catchError(this.handleError<Pokemon>(`getPokemon id=${id}`))
	);
	}
  
	/** PUT: update the pokemon on the server: Mettre à jour le pokemon */
    updatePokemon(pokemon: Pokemon): Observable<Pokemon> {
      const httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
      };

      return this.http.put(this.pokemonsUrl, pokemon, httpOptions).pipe(
        tap(_ => this.log(`updated pokemon id=${pokemon.id}`)),
        catchError(this.handleError<any>('updatedpokemon'))
      );
    }

	/** DELETE pokemon Methode supprimer un pokémon */
	deletePokemon(pokemon: Pokemon): Observable<Pokemon> {
		const url = `${this.pokemonsUrl}/${pokemon.id}`;
		const httpOptions = {
			headers: new HttpHeaders({'Content-Types': 'application/json'})
		};

		return this.http.delete<Pokemon>(url, httpOptions).pipe(
			tap(_ => this.log(`deleted pokemon id=${pokemon.id}`)),
			catchError(this.handleError<Pokemon>('deletedpokemon'))
		);
    }

	/* GET pokemons search */
	searchPokemons(term: string): Observable<Pokemon[]> {
		if(!term.trim()) {
			return of([]);
		}

		return this.http.get<Pokemon[]>(`${this.pokemonsUrl}/?name=${term}`).pipe(
			tap(_ => this.log(`found pokemons matching "${term}"`)),
			catchError(this.handleError<Pokemon[]>('searchPokemons', []))
		);
	}

    // Methode qui permet de retourner tous les types de pokémons
    getPokemonTypes(): string[]{
      return [
		  'Plante', 
		  'Feu', 
		  'Eau', 
		  'Insecte', 
		  'Normal', 
		  'Electrik', 
		  'Poison', 
		  'Fée', 
		  'Vol'
		]
    }

	/** Gestion des erreurs  */
    private log(log: string) {
		console.info(log);
	  }
  
	  private handleError<T>(operation='operation', result?:T) {
		return (error: any): Observable<T> => {
		  console.log(error);
		  console.log(`${operation} failled: ${error.message}`);
  
		  return of(result as T);
		};
	  }

}
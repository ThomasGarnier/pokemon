# Pokemon
<h3><img src="https://raw.githubusercontent.com/angular/angular/master/aio/src/assets/images/logos/angular/angular.png" alt="angular-logo" width="20px" height="20px"/>Pokemon - Une petite application de Pokémons faite sur Angular 9</h3>

## Contenu et étapes de cet exercice
- Créer une application Angular complète à partir d'un dossier vide
- Développer un système de navigation entre composants
- Ajouter des formulaires pilotés par le template et par le modèle
- Effectuer des requêtes HTTP depuis son application
- Mettre en place un système d'authentification
- Déployer une application Angular en production

<p><strong>L'ordre de chargement de l'application est le suivant :</strong><br> index.html > main.ts > app.module.ts > app.component.ts.</p>


## Lancer l'application
```bash
$ npm start
```

## Liens utiles

- <a href="https://angular.io/start" target="_blank" alt="Lien angular.io">Get started in 5 minutes</a>
- <a href="https://cli.angular.io/" target="_blank" alt="Angular CLI">Angular Command Line (CLI)</a>
- <a href="https://material.angular.io/" target="_blank" alt="Angular Material">Angular Material</a>

<p>
  <img src="https://raw.githubusercontent.com/angular/angular/master/docs/images/angular-ecosystem-logos.png" alt="angular ecosystem logos" width="500px" height="auto">
</p>
